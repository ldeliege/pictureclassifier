package be.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FileUtilsTest {

	@Test(expected = RuntimeException.class)
	public void testSanitizeFolderWithFile() {
		FileUtils.sanitizeFolder("CSC_0539.JPG");
	}

	@Test
	public void testEndWith() {
		String folder = "cr2";
		assertEquals("cr2\\", FileUtils.sanitizeFolder(folder));

		folder = "cr2\\";
		assertEquals("cr2\\", FileUtils.sanitizeFolder(folder));
	}

}
