package be.classifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

public class PictureClassifierTest {

	@Test
	public void test() {
		PictureClassifier pictureClassifier = new PictureClassifier();
		File file = new File("IMG_0490.JPG");
		assertTrue(pictureClassifier.getKey(file).isPresent());
		assertEquals("2007\\12\\IMG-13-74027000", pictureClassifier
				.getKey(file).get().getDestination());
		file = new File("CSC_0539.JPG");
		assertTrue(pictureClassifier.getKey(file).isPresent());
		assertEquals("2012\\07\\IMG-23-53184000", pictureClassifier
				.getKey(file).get().getDestination());
	}

	@Test
	public void testExif() {
		PictureClassifier pictureClassifier = new PictureClassifier();
		File file = new File("DSC_0151.JPG");
		pictureClassifier.getKey(file);
	}

	@Test
	public void testCr2() {
		PictureClassifier pictureClassifier = new PictureClassifier();
		File file = new File("cr2\\IMG_4526.CR2");
		PictureKey key1 = pictureClassifier.getKey(file).get();
		file = new File("cr2\\IMG_4526.JPG");
		PictureKey key2 = pictureClassifier.getKey(file).get();

		assertTrue(key1.equals(key2));
	}
}
