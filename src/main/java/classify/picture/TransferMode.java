package classify.picture;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public enum TransferMode {

	Move, Copy;

	public void proceed(Path fromPath, Path toPath) throws IOException {
		if (this == Move) {
			Files.move(fromPath, toPath, StandardCopyOption.COPY_ATTRIBUTES);
		} else {
			Files.copy(fromPath, toPath, StandardCopyOption.COPY_ATTRIBUTES);
		}
	}

}
