package classify.picture;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import be.classifier.Classifier;
import be.classifier.PictureClassifier;
import be.classifier.PictureKey;
import be.utils.FileUtils;

/**
 * Hello world!
 *
 */
public class App {

	private Logger logger = Logger.getLogger(PictureClassifier.class);

	private Classifier<PictureKey> classifier;

	/** Root folders to be analyzed. */
	private String[] sourceFolders;
	/** The destination folder. */
	private String destinationFolder;
	/**
	 * Map of unique entry, key is created using the method computeDestination
	 * of the Key.
	 */
	private HashMap<String, SortedMap<PictureKey, Long>> uniqueEntry = new HashMap<>();
	/** Count the total number of file analyzed by the system. */
	private Integer totalCount = 0;
	/** Test mode, the file will not be moved/copied after the analyze. */
	private boolean testOnly = false;
	/**
	 * Transfer mode: Move, the file are moved in the new folders (no revert
	 * possible, fast), Copy, the file are copied in the new folders
	 */
	TransferMode transferMode = TransferMode.Copy;

	public App(String destinationFolder, String... sourceFolders) {
		this.sourceFolders = new String[sourceFolders.length];
		int i = 0;
		for (String folder : sourceFolders) {
			this.sourceFolders[i] = FileUtils.sanitizeFolder(folder);
			i++;
		}
		this.destinationFolder = FileUtils.sanitizeFolder(destinationFolder);

		classifier = new PictureClassifier();
	}

	/**
	 * Scan each files or folders given as parameter. Folder are managed
	 * recursively.
	 * 
	 * @param files
	 *            List of file or folder to be analyzed.
	 */
	private void scanFiles(File[] files) {
		for (File f : files) {
			if (f.isDirectory()) {
				logger.trace(f.getAbsolutePath());
				scanFiles(f.listFiles());
			} else {
				if (classifier.isFileSupported(f.getName())) {
					totalCount++;
					if (classifier.isFileTooSmall(f)) {
						continue;
					}
					Optional<PictureKey> optionalKey = classifier.getKey(f);
					if (optionalKey.isPresent()) {
						analysePicture(f, optionalKey.get());
					} else {
						logger.debug("No key for picture "
								+ f.getAbsolutePath());
					}
				}
			}
		}
	}

	private void analysePicture(File f, PictureKey key) {
		SortedMap<PictureKey, Long> destinationMap = getDestinationMap(key);

		if (destinationMap.containsKey(key)) {
			if (isNewFileBigger(f, key, destinationMap)) {
				destinationMap.put(key, f.length());
			}
		} else {
			destinationMap.put(key, f.length());
		}
	}

	private SortedMap<PictureKey, Long> getDestinationMap(PictureKey key) {
		SortedMap<PictureKey, Long> destinationMap = uniqueEntry.get(key
				.computeDestination());
		if (destinationMap == null) {
			destinationMap = new TreeMap<>();
			uniqueEntry.put(key.computeDestination(), destinationMap);
		}
		return destinationMap;
	}

	private boolean isNewFileBigger(File f, PictureKey key,
			SortedMap<PictureKey, Long> destinationMap) {
		return destinationMap.get(key) < f.length();
	}

	public void classify() {
		for (String sourceFolder : sourceFolders) {
			File[] listFiles = new File(sourceFolder).listFiles();
			if (listFiles != null) {
				scanFiles(listFiles);
			}
		}

		if (!testOnly) {
			transferFiles();
		}

		logger.debug("total Count " + totalCount);
		long totalunique = uniqueEntry.values().stream()
				.mapToInt(el -> el.size()).sum();

		logger.debug("unique entry " + totalunique);
	}

	/**
	 * 
	 */
	private void transferFiles() {
		for (Entry<String, SortedMap<PictureKey, Long>> entry : uniqueEntry
				.entrySet()) {
			try {
				Files.createDirectories(getAbsoluteDestinationPath(
						destinationFolder, entry.getKey(), Optional.empty()));
				int pictureNumber = 1;
				for (PictureKey key : entry.getValue().keySet()) {
					transferMode.proceed(
							key.getOriginalPath(),
							getAbsoluteDestinationPath(destinationFolder, entry
									.getKey(), Optional.of(generateFileName(
									pictureNumber, key))));
					pictureNumber++;
				}

			} catch (IOException e) {
				logger.error("Error while copying/moving the file", e);
			}

		}
	}

	private String generateFileName(int pictureNumber, PictureKey key) {
		return "IMG-" + StringUtils.leftPad(pictureNumber + "", 7, "0") + "."
				+ key.getOriginalFileType();
	}

	private Path getAbsoluteDestinationPath(String mainDestinationFolder,
			String subFolder, Optional<String> filename) {
		if (filename.isPresent()) {
			return Paths.get(mainDestinationFolder + File.separator + subFolder
					+ File.separator + filename.get());
		} else {
			return Paths
					.get(mainDestinationFolder + File.separator + subFolder);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out
					.println("Two arguments are required: source dir and destination dir.");
			System.exit(1);
		}

		App classify = new App(args[1], args[0].split(File.pathSeparator));

		classify.classify();

	}

}
