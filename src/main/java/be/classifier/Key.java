package be.classifier;

import java.io.File;

public interface Key<K> extends Comparable<K> {

	/**
	 * Get the destination based for the specific key.
	 * 
	 * the default implementation use the compute destination method (akka
	 * folder and subfolder) + the original filename.
	 * 
	 * @return the folder where the file must be moved to.
	 */
	default public String getDestination() {
		return computeDestination() + File.separator + getOriginalFilename();
	}

	/**
	 * Compute the destination folder. The result shouldn't contain the
	 * filename, one or several folder. If multiple folder are returned, the
	 * result should respect the structure: folder1/folder2/folder3 where / is
	 * the File.separator.
	 * 
	 * @return the full computed destination folder.
	 */
	public String computeDestination();

	/**
	 * Get the original filename of the key.
	 * 
	 * @return the original filename
	 */
	public String getOriginalFilename();

	/**
	 * Get the original file type of the key
	 * 
	 * @return the original file type
	 */
	public String getOriginalFileType();

}
