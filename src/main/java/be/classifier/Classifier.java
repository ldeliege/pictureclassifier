package be.classifier;

import java.io.File;
import java.util.Optional;

public interface Classifier<K extends Key<K>> {

	/**
	 * Check if the file is managed by the implementation.
	 * 
	 * @param filename
	 *            the filename
	 * @return true if the implementation supported this type of file.
	 */
	public boolean isFileSupported(String filename);

	/**
	 * Get the instance of Key for the specified file.
	 * 
	 * @param file
	 *            the file
	 * @return the instance of Key
	 */
	public Optional<K> getKey(File file);

	/**
	 * Check the size of the file.
	 * 
	 * @param f
	 *            the file
	 * @return true if the size of the file is less or equals than a specific
	 *         size, depending on the type of classifier
	 */
	public boolean isFileTooSmall(File f);

}
