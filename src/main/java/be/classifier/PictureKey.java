package be.classifier;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class PictureKey implements Key<PictureKey> {

	private String originalFileName;
	private ExifSubIFDDirectory exifInformation;
	private Optional<DateTime> shootingDate = Optional.empty();
	private DateTimeFormatter fmt = DateTimeFormat
			.forPattern("yyyy:MM:dd HH:mm:ss");
	private Path originalPath;

	private static List<Integer> ignoredExifTags = Arrays
			.asList(new Integer[] { 37121 });

	public PictureKey(String originalFileName, Path originalPath,
			long lastModifiedDate, ExifSubIFDDirectory exifInformation) {
		this.originalFileName = originalFileName;
		this.exifInformation = exifInformation;
		this.originalPath = originalPath;

		for (Tag tag : exifInformation.getTags()) {
			if (tag.getTagType() == com.drew.metadata.exif.ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL) {
				String dateTimeOriginal = tag.getDescription();
				if (StringUtils.isEmpty(dateTimeOriginal)) {
					shootingDate = Optional.empty();
				} else {
					shootingDate = Optional.of(fmt
							.parseDateTime(dateTimeOriginal));
				}
			}
		}
		if (!shootingDate.isPresent()) {
			shootingDate = Optional.of(new DateTime(lastModifiedDate));
		}
	}

	public Path getOriginalPath() {
		return originalPath;
	}

	@Override
	public String toString() {
		return originalFileName + shootingDate;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object key) {
		if (key instanceof PictureKey) {
			PictureKey otherKey = (PictureKey) key;
			if (otherKey.exifInformation.getTagCount() == this.exifInformation
					.getTagCount()) {
				return areTagsEqual(otherKey);
			}
		}
		return false;
	}

	private boolean areTagsEqual(PictureKey otherKey) {
		for (Tag thisTag : this.exifInformation.getTags()) {
			Object value = this.exifInformation.getObject(thisTag.getTagType());
			Object otherValue = otherKey.exifInformation.getObject(thisTag
					.getTagType());
			if (mustCompareTag(thisTag) && !isSameValues(value, otherValue)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Some Exif tags must be ignored because they are not relevant.
	 * <ul>
	 * <li>ComponentsConfiguration 37121 : Specific to compressed data, not
	 * relevant</li>
	 * </ul>
	 * to compare JPeg and Raw format.
	 * 
	 * @param thisTag
	 *            the current tag
	 * @return true if the tag is not part of the ignoredExifTags list
	 */
	private boolean mustCompareTag(Tag thisTag) {
		return !ignoredExifTags.contains(thisTag.getTagType());
	}

	private boolean isSameValues(Object value, Object otherValue) {
		if (value instanceof Object[] && otherValue instanceof Object[]) {
			return Arrays.equals((Object[]) value, (Object[]) otherValue);
		}
		if (value instanceof byte[] && otherValue instanceof byte[]) {
			return Arrays.equals((byte[]) value, (byte[]) otherValue);
		}
		if (value instanceof int[] && otherValue instanceof int[]) {
			return Arrays.equals((int[]) value, (int[]) otherValue);
		}
		if (value instanceof float[] && otherValue instanceof float[]) {
			return Arrays.equals((float[]) value, (float[]) otherValue);
		}
		if (value instanceof long[] && otherValue instanceof long[]) {
			return Arrays.equals((long[]) value, (long[]) otherValue);
		}

		return (value != null && value.equals(otherValue))
				|| (value == null && otherValue == null);
	}

	@Override
	public String getDestination() {
		if (shootingDate.isPresent()) {
			return computeDestination() + File.separator + "IMG-"
					+ shootingDate.get().getDayOfMonth() + "-"
					+ shootingDate.get().getMillisOfDay();
		} else {
			throw new RuntimeException(
					"shooting date is mandatory to get the Destination");
		}
	}

	@Override
	public String computeDestination() {
		if (shootingDate.isPresent()) {
			int month = shootingDate.get().get(DateTimeFieldType.monthOfYear());
			int year = shootingDate.get().get(DateTimeFieldType.year());
			return year + File.separator + (month < 10 ? "0" + month : month);
		} else {
			throw new RuntimeException(
					"shooting date is mandatory to get the computeDestination");
		}
	}

	@Override
	public String getOriginalFilename() {
		return originalFileName;
	}

	@Override
	public String getOriginalFileType() {
		return FilenameUtils.getExtension(originalFileName);
	}

	@Override
	public int compareTo(PictureKey otherPictureKey) {
		Optional<DateTime> otherShootingDate = otherPictureKey.shootingDate;
		if (shootingDate.isPresent() && otherShootingDate.isPresent()) {
			return (shootingDate.get().compareTo(otherShootingDate.get()));
		} else if (shootingDate.isPresent() && !otherShootingDate.isPresent()) {
			return 1;
		} else if (!shootingDate.isPresent() && otherShootingDate.isPresent()) {
			return -1;
		} else {
			return 0;
		}
	}

}
