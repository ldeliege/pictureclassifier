package be.classifier;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.joda.time.IllegalFieldValueException;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class PictureClassifier implements Classifier<PictureKey> {

	private Logger logger = Logger.getLogger(PictureClassifier.class);

	public PictureClassifier() {

	}

	@Override
	public Optional<PictureKey> getKey(File file) {
		try {
			Metadata metadata = ImageMetadataReader.readMetadata(file);
			Collection<ExifSubIFDDirectory> exifs = metadata
					.getDirectoriesOfType(ExifSubIFDDirectory.class);
			if (exifs != null) {
				if (exifs.size() > 1) {
					throw new RuntimeException(
							"too many exifSubIFDDirectory, please check");
				} else if (exifs.size() == 1) {
					for (ExifSubIFDDirectory exif : exifs) {
						return Optional.of(new PictureKey(file.getName(), file
								.toPath(), file.lastModified(), exif));
					}
				}
			}
		} catch (ImageProcessingException | IOException | NullPointerException
				| IllegalFieldValueException e) {
			logger.error("Error while processing the key", e);
		}
		logger.debug("No exifs information for file " + file.getAbsolutePath());
		return Optional.empty();
	}

	@Override
	public boolean isFileSupported(String filename) {
		String lowerCase = filename.toLowerCase();

		return lowerCase.endsWith(".jpg") || lowerCase.endsWith(".jpeg")
				|| lowerCase.endsWith(".cr2");
	}

	public String padMonth(int month) {
		if (month < 10) {
			return "0" + month;
		} else {
			return month + "";
		}
	}

	/**
	 * Check the size of the file. A file is considered as too small if the size
	 * is less or equals to 100kb. It means it is probably a thumbnail.
	 * 
	 * @param f
	 *            the file
	 * @return true if the size of the file is less or equals than 100kb
	 */
	@Override
	public boolean isFileTooSmall(File f) {
		return f.length() <= 100 * 1024;
	}
}
