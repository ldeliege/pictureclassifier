package be.utils;

import java.io.File;

public class FileUtils {

	public static String sanitizeFolder(String folder) {
		File f = new File(folder);

		if (f.isFile()) {
			throw new RuntimeException(folder
					+ " is a file, destination must be a directory");
		}

		if (folder.endsWith(File.separator)) {
			return folder;
		} else {
			return folder + File.separator;
		}
	}

}
