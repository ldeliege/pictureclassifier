# README #

Picture classifier.
The purpose of the application is to read the Exif information of picture and to classify the picture using the information.
Two pictures are considered as equals if the Exif information is the same (with some exceptions).  The application move or copy the pictures into a destination folder.  All pictures are renamed and ordered by 

* Version
0.01 - initial version.

### How do I get set up? ###

* Summary of set up

Just import the Maven project into your IDE.

* Configuration
To start the application, launch

be.classifier.PictureClassifierTest

with two parameters:

Param1: the different path to be analized, separated using the path separator of your OS;
Param2: the destination folder, where the file will be copy/moved.

* Dependencies
See pom.xml

* How to run tests

Unit testing must still be fully defined.  Must be executed from the working directory: ${workspace_loc:picture/target/test-classes}

* To Do

- incremental update
- allow to choose between test and copy mode
- allow to choose between copy or move mode
- better management of picture with poor Exif entries
- unit testing
- better feedback during execution

### Contribution guidelines ###
None.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact